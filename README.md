# Missing HUD 2 Bot
This bot works on /r/BindingofIsaac and /r/TheModdingofIsaac. This is the source code of it, with stripped OAuth login data.

This bot is based on [ReplyBot](https://github.com/voussoir/reddit/tree/master/ReplyBot) by [voussoir](https://github.com/voussoir).

Warning: you will have to use OAuth for this bot. Please read [this](https://www.reddit.com/comments/3cm1p8/how_to_make_your_bot_use_oauth2/) Reddit post for instructions. The file `obot_base.py` provided here serves as a base for obot.py. You can rename it to `obot.py` and change all the values. You shouldn't have to change the scopes, though, unless you want to add functionality to the bot other than simply replying with a preset message to preset keywords.

There is a shell script called `keywords.sh` in this repository. You can run it with the Bash interpreter to return a proper Markdown-formatted list of keywords taken directly from the `KEYWORDS` table in `replybot.py`. It's used to generate the [/r/MissingHUD2Bot's wiki page](https://www.reddit.com/r/MissingHUD2Bot/wiki/index).
