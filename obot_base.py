app_id = ""
app_secret = ""
app_uri = ''
app_ua = ""
app_scopes = 'identify submit read history' #These are the scopes you need to have the bot read and reply to comments.
app_account_code = ""
app_refresh = ""

#If you don't know how to fill these in:
#https://www.reddit.com/comments/3cm1p8/how_to_make_your_bot_use_oauth2/
