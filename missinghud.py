#/u/GoldenSights
import traceback
import praw # simple interface to the reddit API, also handles rate limiting of requests
import obot # move obot_base.py to obot.py and change the values
import time
import sqlite3

'''USER CONFIGURATION'''

APP_ID = obot.app_id
APP_SECRET = obot.app_secret
APP_URI = obot.app_uri
APP_REFRESH = obot.app_refresh
# https://www.reddit.com/comments/3cm1p8/how_to_make_your_bot_use_oauth2/
USERAGENT = obot.app_ua
# This is a short description of what the bot does.
# For example "Python automatic replybot v2.0 (by /u/GoldenSights)"
SUBREDDIT = "BindingofIsaac+MissingHUD2Bot+TheModdingOfIsaac"
# This is the sub or list of subs to scan for new posts. For a single sub, use "sub1". For multiple subreddits, use "sub1+sub2+sub3+..."
DO_SUBMISSIONS = True
DO_COMMENTS = True
# Look for submissions, comments, or both.
KEYWORDS = ["@MissingHUD", "stats on left side", "stats on the left side", "stats info", "stats information", "stats list", "statistics list", "stat hud", "statistic hud", "left side hud", "left side stats", "left stats side", "left side stat", "left hud", "left stats", "stats on left", "numbers on the left", "numbers on left", "numbers on the left side", "left side numbers", "side of the screen", "bottom-left area of the screen", "left bottom of the screen", "screen left bottom", "stats on screen", "screen stats", "stats appear on the side", "stats that appear", "stats that appear on the side", "hud on the side", "stats on the side", "hud on the side", "side hud", "stats on the left", "stats on the middle left", "middle left stats", "stats on the left middle", "left middle stats", "stats to show up", "stats show up", "stats left middle", "stats to show up on the middle left"]
# These are the words you are looking for
KEYAUTHORS = []
# These are the names of the authors you are looking for
# The bot will only reply to authors on this list
# Keep it empty to allow anybody.
REPLYSTRING = "##Hey! The stats on the left side come from a mod called Missing HUD 2. You can download it [here](https://github.com/networkMe/missinghud2/releases). It works on Windows only.\n\n---\n\nI'm a bot. This action was performed automatically. If something bad happened, [send the direct link to this post to Zatherz](https://www.reddit.com/message/compose?to=Zatherz&subject=MissingHUD2%20Bot&message=[Replace%20this%20with%20a%20direct%20link%20to%20the%20errorneous%20comment])."
# This is the word you want to put in reply
MAXPOSTS = 100
# This is how many posts you want to retrieve all at once. PRAW can download 100 at a time.
WAIT = 2
# This is how many seconds you will wait between cycles. The bot is completely inactive during this time.

'''All done!'''

try:
    import bot
    USERAGENT = bot.aG
except ImportError:
    pass

print('Opening SQL Database')
sql = sqlite3.connect('sql.db')
cur = sql.cursor()
cur.execute('CREATE TABLE IF NOT EXISTS oldposts(id TEXT)')

print('Logging in...')
r = praw.Reddit(USERAGENT)
r.set_oauth_app_info(APP_ID, APP_SECRET, APP_URI)
r.refresh_access_information(APP_REFRESH)

def replybot():
    print('Searching %s.' % SUBREDDIT)
    subreddit = r.get_subreddit(SUBREDDIT)
    posts = []
    if DO_SUBMISSIONS:
        posts += list(subreddit.get_new(limit=MAXPOSTS))
    if DO_COMMENTS:
        posts += list(subreddit.get_comments(limit=MAXPOSTS))
    posts.reverse()

    for post in posts:
        # Anything that needs to happen every loop goes here.
        pid = post.id
        
        try:
            pauthor = post.author.name
        except AttributeError:
            # Author is deleted. We don't care about this post.
            continue

        if pauthor.lower() == r.user.name.lower() or pauthor.lower() == "missinghudbot":
            # Don't reply to yourself, robot!
            continue

        if KEYAUTHORS != [] and all(auth.lower() != pauthor for auth in KEYAUTHORS):
            # This post was not made by a keyauthor
            continue

        cur.execute('SELECT * FROM oldposts WHERE ID=?', [pid])
        fetch = cur.fetchone()

        if pid == "4gpmuv":
                print(pid, fetch)

        if fetch:
            # Post is already in the database
            continue

        if isinstance(post, praw.objects.Comment):
            pbody = post.body
        else:
            pbody = '%s %s' % (post.title, post.selftext)
        pbody = pbody.lower()

        if not any(key.lower() in pbody for key in KEYWORDS):
            # Does not contain our keyword
            continue
        

        print('Replying to %s by %s (DB: %s)' % (pid, pauthor, fetch))
        exit()
        try:
            if hasattr(post, "reply"):
                post.reply(REPLYSTRING)
            else:
                post.add_comment(REPLYSTRING)
            cur.execute('INSERT INTO oldposts VALUES(?)', [pid])
            sql.commit()
        except praw.errors.Forbidden:
            print('403 FORBIDDEN - is the bot banned from %s?' % post.subreddit.display_name)

cycles = 0
while True:
    try:
        replybot()
        cycles += 1
        print("Cycle %s" % (cycles))
    except Exception as e:
        traceback.print_exc()
    print('Running again in %d seconds \n' % WAIT)
    time.sleep(WAIT)

    
